using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverPNG : MonoBehaviour
{
    public PlayerData pdata;
    enum direcciones { stop, derecha, izquierda }

    private direcciones direccion;
    public Vector3 pop;

    private void Awake()
    {
        pdata = GetComponent<PlayerData>();

        pop = new Vector3(0.08f * Time.deltaTime, 0f, 0f);
    }
    void Start()
    {
        direccion = direcciones.izquierda;
    }


    void Update()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        if (direccion == direcciones.izquierda)
        {
            transform.position -= pop;
            if (transform.position.x < -6.5f)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                direccion = direcciones.derecha;

                
            }
        }
        else if (direccion == direcciones.derecha)
        {
            transform.position += pop;
            if (transform.position.x > 6.5f)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
                direccion = direcciones.izquierda;
            }
        }
    }
}
