using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gegantisme : MonoBehaviour
{
    float x;
    float y;
    Vector3 newSize;
    public PlayerData pdata;
    public int giantTime = 15;
    public float tiempo_start;
    public float tiempo_end = 4;
    public float coldownStart = 4;
    public float coldownEnd = 4;
    public bool flag;

    private void Start()
    {
        flag = false;
    }
    void Update()
    {
        if (!flag)
        {
            tiempo_start = 0;

            if (coldownStart >= coldownEnd)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    x = 10;
                    y = 10;
                    this.transform.localScale += new Vector3(x, y, 0);
                    flag = true;
                }
            }
            else
            {
                coldownStart += Time.deltaTime;
            }  
        }
        else
        {
            if (tiempo_start >= tiempo_end)
            {
                x = 6;
                y = 6;
                this.transform.localScale = new Vector3(x, y, 0);
                coldownStart = 0;
                flag = false;
            }
            else
            {
                tiempo_start += Time.deltaTime;
            }
        }
    }
}
